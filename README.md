Single-Layer Perceptron: From data creation to perceptron training.

This project contains:

1. Physical data creation and plotting using R.
2. Single-Layer Perceptron using Python.
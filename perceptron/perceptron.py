import matplotlib.pyplot as plt
import numpy as np
import math
import random

# Global variables
dataFile = open("data.txt")
height_m = []
weight_m = []
height_f = []
weight_f = []
neuron_weights = [0, 0, 0]
t_negative = 0
f_negative = 0
t_positive = 0
f_positive = 0
learning_rate = 0.2


def plot(title):
    # Scatter plot
    plt.scatter(height_m, weight_m, s=5, c='b', alpha=0.7, marker=r'o', label='Male')
    plt.scatter(height_f, weight_f, s=5, c='r', alpha=0.7, marker=r'o', label='Female')
    plt.title(title)
    plt.xlabel('Height(ft)')
    plt.ylabel('Weight(lb)')
    plt.legend()

    # Decision line
    x_range = np.array(range(0, 2))
    argument = str(-(neuron_weights[0] / neuron_weights[1])) + '*x_range+' + str(
        -(neuron_weights[2] / neuron_weights[1]))
    y_range = eval(argument)
    plt.plot(x_range, y_range, c='g', alpha=0.5)

    plt.show()


def activation(activation_type, sex, net):
    if activation_type:
        # Overflow
        # return 1 / (1 + math.exp(-1 * net))
        return 0
    else:
        if sex == 'M':
            if net > 0:
                return 1
            elif net == 0:
                return 0
            else:
                return -1
        else:
            return -1 if net < 0 else 1


def test(sample):
    global t_positive, f_negative, t_negative, f_positive, accuracy, error
    for index in range(sample, 2000):
        net = (height_m[index] * neuron_weights[0]) + (weight_m[index] * neuron_weights[1]) + neuron_weights[2]

        if net >= 0:
            t_positive += 1
        elif net < 0:
            f_negative += 1

        net = (height_f[index] * neuron_weights[0]) + (weight_f[index] * neuron_weights[1]) + neuron_weights[2]

        if net < 0:
            t_negative += 1
        elif net >= 0:
            f_positive += 1
    accuracy = (t_positive + t_negative) / (t_negative + t_positive + f_negative + f_positive)
    error = 1 - accuracy


def train(sample, epoch, activation_type):
    global neuron_weights
    neuron_weights = [random.randint(-1, 1), random.randint(-1, 0), random.randint(190, 210)]
    iteration = 0
    total_error = 1

    if activation_type:
        name = "Soft activation"
    else:
        name = "Hard activation"

    while total_error > math.pow(10, -5) and iteration < epoch:
        iteration += 1
        total_error = 0

        for i in range(sample):
            net = (height_m[i] * neuron_weights[0]) + (weight_m[i] * neuron_weights[1]) + neuron_weights[2]
            actual = activation(activation_type, 'M', net)
            error = (1 - actual)
            steer = learning_rate * error

            neuron_weights[0] += steer * height_m[i]
            neuron_weights[1] += steer * weight_m[i]
            neuron_weights[2] += steer

            total_error += 1 / 4000 if net < 0 else 0

            net = (height_f[i] * neuron_weights[0]) + (weight_f[i] * neuron_weights[1]) + neuron_weights[2]
            actual = activation(activation_type, 'F', net)
            error = (-1 - actual)
            steer = learning_rate * error

            neuron_weights[0] += steer * height_f[i]
            neuron_weights[1] += steer * weight_f[i]
            neuron_weights[2] += steer

            total_error += 1 / 4000 if net >= 0 else 0

    test(sample)
    output()
    plot("Plot for {0} sample data {1}".format(sample, name))


def output():
    print("\n")
    print("Final weight: ", neuron_weights)
    print("True Positive: ", t_positive)
    print("False Positive: ", f_positive)
    print("True Negative: ", t_negative)
    print("False Negative: ", f_negative)
    print("Accuracy: ", accuracy)
    print("Error: ", error)
    print("\n")


def scale(arr):
    temp = []
    i = 0
    mini = min(arr)
    maxi = max(arr)
    denom = maxi - mini
    while i < len(arr):
        temp.append((arr[i] - mini) / denom)
        i += 1
    return temp


# Get data from 1st project
def load():
    global height_m, weight_m, height_f, weight_f
    for _ in range(2000):
        data = dataFile.readline().split(",")
        height_m.append(round(float(data[0]), 2))
        weight_m.append(round(float(data[1]), 2))

    for _ in range(2000):
        data = dataFile.readline().split(",")
        height_f.append(round(float(data[0]), 2))
        weight_f.append(round(float(data[1]), 2))

    height_m = scale(height_m)
    weight_m = scale(weight_m)
    height_f = scale(height_f)
    weight_f = scale(weight_f)

    # 90%
    # train(1800, 3000, 0.3, False)
    # 75% hard
    train(1500, 1000, False)
    # 25% hard
    train(500, 1000, False)
    # 75% soft
    train(1500, 5000, True)
    # 25% soft
    train(500, 5000, True)


load()
